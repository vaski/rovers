# Class for handle drone instructions
# and calculate drone stop position
class Route
  # Regexp for validate drone instruction
  # allow values like 'RMMLLMMMRL'
  ROUTE_INPUT_REGEXP = /\A[LRM]+\z/

  # Constructor for new object
  # gets and parse drone instruction input
  # calculate drone stop position
  def initialize(plateau, start_position)
    @plateau, @start_position = plateau, start_position
    get_input
  end

  def stop_position
    "#{@x} #{@y} #{@heading}"
  end

  private

    def get_input
      puts_input_instructions
      read_input
      process_input
    end

    def puts_input_instructions
      puts "\nPlease enter a instructions for drone and press ENTER"
      puts "instructions is a string of letters, possible letters are `L`, `R` and `M`"
    end

    def read_input
      @input = gets.strip
    end

    def process_input
      check_input_format
      run_command
    end

    def check_input_format
      unless @input =~ ROUTE_INPUT_REGEXP
        puts_invalid_format_error
        get_input
      end
    end

    def run_command
      @x, @y = @start_position.x, @start_position.y
      @heading = @start_position.heading

      @input.each_char do |command|
        send(command.downcase)

        if command == 'M' && position_out_of_plateau?
          puts_out_of_plateau_error
          get_input
          break
        end
      end
    end

    def m
      case @heading
      when 'N'
        @y += 1
      when 'E'
        @x += 1
      when 'S'
        @y -= 1
      when 'W'
        @x -= 1
      end
    end

    def r
      case @heading
      when 'N'
        @heading = 'E'
      when 'E'
        @heading = 'S'
      when 'S'
        @heading = 'W'
      when 'W'
        @heading = 'N'
      end
    end

    def l
      case @heading
      when 'N'
        @heading = 'W'
      when 'E'
        @heading = 'N'
      when 'S'
        @heading = 'E'
      when 'W'
        @heading = 'S'
      end
    end

    def position_out_of_plateau?
      !((0..@plateau.x).include?(@x) && (0..@plateau.y).include?(@y))
    end

    def puts_invalid_format_error
      puts "Error: `#{@input}` have invalid format"
    end

    def puts_out_of_plateau_error
      puts "Error: `#{@input}` is out of a plateau"
    end
end
