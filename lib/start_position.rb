# Class for handle and store info about rover start position
class StartPosition
  # Regexp for validate rover start position input
  # allow values like '2 3 N'
  START_POSITION_INPUT_REGEXP = /\A\d+\s+\d+\s+[NWSE]\z/

  attr_reader :x, :y, :heading

  # Constructor for new object
  # gets and parse rover start position input
  # set position data into instance variables
  def initialize(plateau)
    @plateau = plateau
    get_input
  end

  private
    def get_input
      puts_input_instructions
      read_input
      check_input
    end

    def puts_input_instructions
      puts "\nPlease enter a start position of drone as `x y z` and press ENTER"
      puts "enter `x` and `y` as positive integer or zero"
      puts "max value for `x` is #{@plateau.x} for `y` is #{@plateau.y}"
      puts "enter `z` as heading derection, available values is `N`,`W`,`S`,`E`"
    end

    def read_input
      @input = gets.strip
    end

    def check_input
      check_input_format
      parse_input
      check_position
    end

    def check_input_format
      unless @input =~ START_POSITION_INPUT_REGEXP
        puts_invalid_format_error
        get_input
      end
    end

    def parse_input
      @x, @y, @heading = *@input.split
      @x, @y = @x.to_i, @y.to_i
    end

    def check_position
      unless (0..@plateau.x).include?(@x) && (0..@plateau.y).include?(@y)
        puts_out_of_plateau_error
        get_input
      end
    end

    def puts_invalid_format_error
      puts "Error: `#{@input}` have invalid format"
    end

    def puts_out_of_plateau_error
      puts "Error: `#{@input}` is out of a plateau"
    end
end
