# Class for handle and store info about plateau
class Plateau
  # Regexp for validate plateau input
  # allow values like '5 5'
  PLATEAU_INPUT_REGEXP = /\A\d+\s+\d+\z/

  attr_reader :x, :y

  # Constructor for new object
  # gets and parse plateau size input
  # set plateau size into instance variables
  def initialize
    get_input
    @x, @y = *@input.split.map(&:to_i)
  end

  private

    def get_input
      puts_input_instructions
      read_input
      retry_get_input unless @input =~ PLATEAU_INPUT_REGEXP
    end

    def puts_input_instructions
      puts "\nPlease enter a plateau size as `x y` and press ENTER"
      puts "enter `x` and `y` as positive integer or zero"
    end

    def read_input
      @input = gets.strip
    end

    def retry_get_input
      puts_error_message
      get_input
    end

    def puts_error_message
      puts "ERROR: #{@input} have invalid format"
    end
end
