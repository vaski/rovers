require 'minitest/autorun'
require 'stringio'
require_relative '../lib/plateau'
require_relative '../lib/start_position'
require_relative '../lib/route'

describe Plateau do
  before do
    $stdout = StringIO.new

    $stdin = StringIO.new("5 5")
    plateau = Plateau.new

    $stdin = StringIO.new("1 3 N")
    start_position = StartPosition.new(plateau)

    $stdin = StringIO.new("LMLMLMLMM")
    @route = Route.new(plateau, start_position)
  end

  after do
    $stdout = STDOUT
    $stdin = STDIN
  end

  describe '#stop_position' do
    it { @route.stop_position.must_equal '1 4 N' }
  end
end
