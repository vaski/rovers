require 'minitest/autorun'
require 'stringio'
require_relative '../lib/plateau'

describe Plateau do
  before do
    $stdout = StringIO.new
    $stdin = StringIO.new("5 7")
    @plateau = Plateau.new
  end

  after do
    $stdout = STDOUT
    $stdin = STDIN
  end

  describe '#x' do
    it { @plateau.x.must_equal 5 }
  end

  describe '#y' do
    it { @plateau.y.must_equal 7 }
  end
end
