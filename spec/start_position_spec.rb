require 'minitest/autorun'
require 'stringio'
require_relative '../lib/plateau'
require_relative '../lib/start_position'

describe Plateau do
  before do
    $stdout = StringIO.new

    $stdin = StringIO.new("5 5")
    plateau = Plateau.new

    $stdin = StringIO.new("2 3 W")
    @start_position = StartPosition.new(plateau)
  end

  after do
    $stdout = STDOUT
    $stdin = STDIN
  end

  describe '#x' do
    it { @start_position.x.must_equal 2 }
  end

  describe '#y' do
    it { @start_position.y.must_equal 3 }
  end

  describe '#heading' do
    it { @start_position.heading.must_equal 'W' }
  end
end
